<%@ page import="java.sql.*" %>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>

<!--cp /var/lib/tomcat9/webapps/ROOT/META-INF/lib/mysql-connector-java-8.0.28.jar /var/lib/tomcat9/lib/ -->

<a href="add_form.jsp" class="btn btn-primary">Add Employee</a>

		<table class="table table-striped table-hover">
				
		<tbody>
<%		
try{
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/alpha","diituser","%TGBbgt5");

		Statement stmt=con.createStatement();
		ResultSet rs=stmt.executeQuery("select * from employees ORDER BY id DESC");
		int sum = 0;
		while(rs.next()) {
			sum+=rs.getInt("salary");
		%>
		<tr>
			<td><%= rs.getInt("id")%></td>
			<td><%= rs.getString("name")%></td>
			<td><%= rs.getString("address")%></td>
			<td><%= rs.getInt("salary")%></td>
			<td><a class="btn btn-danger"  onClick="return confirm('Are you sure you want to delete details of <%= rs.getString("name") %>')" href="delete.jsp?id=<%= rs.getInt("id")%>">Delete</a>
			<a class="btn btn-info" href="edit_form.jsp?id=<%= rs.getInt("id")%>">Edit</a></td>
		</tr>





		<%	}%>

		</tbody>
		<thead>		
		<th>id</th>
		<th>Name</th>
		<th>Address</th>
		<th>Salary (Total<%= sum %>)</th>
		<th>Action</th>
		</thead>

		</table>
	
		<%	con.close();

} catch(Exception e) { 
	out.println(e);

}


%>
