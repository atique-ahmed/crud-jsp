<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<form action="add_action.jsp">
<div class="container">
  <div class="mb-3">
    <input type="text" name="name" placeholder="Name" class="form-control" >
  </div>
  <div class="mb-3">
    <input type="text" name="address" placeholder="Address" class="form-control" >
  </div>
  <div class="mb-3">
    <input type="text" name="salary" placeholder="Salary" class="form-control" >
  </div>
  <input class="btn btn-primary" type="submit" value="ADD">
  <a class="btn btn-secondary" href="listing.jsp">Cancel</a>
  
  </div>
</form>
